# `tundra` 

`tundra` is a [brutalist](https://brutalist-web.design/), [nord](https://nordtheme.com)ic theme for Python-based [Pelican](https://getpelican.com) static site generator. 

See my development blog [rwev.dev](https://rwev.dev) to see `tundra` in action

## configuration

This repository includes [`tundraconf.py`](https://gitlab.com/rwev/tundra/blob/master/tundraconf.py) where default theme configuration is stored. 

Import the configuration from `tundraconf.py` into your site's `pelicanconf.py` to  provide them to the Pelican's generation context. Assuming this repository in cloned and in your site's directory:
 
 ```[python]
# pelicanconf.py
sys.path.append(os.curdir)
from tundra import *
```

Overwrite any of the variables in your `pelicanconf.py` after this import, whether they be static template display strings or plugin configuration.
 
 See the example [`pelicanconf.py`](https://gitlab.com/rwev/rwev.gitlab.io/blob/master/pelicanconf.py) as reference.
 
## plugin support

`tundra`'s templates and base configuration support the following plugins out of the box:

  - "autopages",
  - "neighbors",
  - "more_categories",
  - "summary"

